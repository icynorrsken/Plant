# Plant
## Tester Tool

## Projet Génie Logiciel

**Projet GL Ensimag Grenoble INP JUIN 2015**

## Team : Les bûcherons d'AST

* Thomas Bianchini
* Tom Lasne
* Jerôme Barbier
* Iliyas Jorio
* Thibault Batal
* Jean-Benjamin Roussau
* Jean de la Fayolle
* Rémi Saurel

## Description

Outil de test permettant de valider un grand nombre de test sur le compilateur Deca.

Plant permet de lancer des scripts et de récupérer leur résultat. Ces résultats sont ensuite traités et affichés dans un rapport console ou web (selon les options).

## Développeurs

**Partie script bash** : Thomas Bianchini et Rémi Saurel

**Partie Java** : Tom Lasne et Thomas Bianchini

## Arguments

**Comportement par défaut**

Si Plant est lancé sans aucun argument, tous les tests de syntax, context, codegen et decompile sont lancés.

Plant fonctionnera en mode multi-threading.

Il va créer un rapport mergé en mode console.

Un rapport mergé est un rapport dans lequel les statistiques sont affichées à la fin du rapport (et non à la fin de chaque catégorie de test).

De plus, des statistiques globales sont générées.

Les rapports web contiennent des informations d'erreur, pour les voir il faut cliquer sur le nom du fichier (ou sur le mot FAILED).

**Options disponibles**

* **web** : permet de généré un rapport web
* **syntax** : ne lance que les tests du répertoire syntax
    * **-s** *(sourcefile or folder)* *: cette argument est optionnel, il permet de sélectionner plus précisement un fichier (ou plusieurs) ou un dossier (ou plusieurs)
     à tester **Attention les sourcefiles ou folders doivent être donnés en chemin absolue**
* **context** : ne lance que les tests du répertoire context
    * **-s** *(sourcefile or folder)* *: cette argument est optionnel, il permet de sélectionner plus précisement un fichier (ou plusieurs) ou un dossier (ou plusieurs)
     à tester **Attention les sourcefiles ou folders doivent être donnés en chemin absolue**
* **codegen** : ne lance que les tests du répertoire codegen
    * **-s** *(sourcefile or folder)* *: cette argument est optionnel, il permet de sélectionner plus précisement un fichier (ou plusieurs) ou un dossier (ou plusieurs)
     à tester **Attention les sourcefiles ou folders doivent être donnés en chemin absolue**
* **decompile** : ne lance que les tests du répertoire codegen pour en faire une décompilation
    * **-s** *(sourcefile or folder)* *: cette argument est optionnel, il permet de sélectionner plus précisement un fichier (ou plusieurs) ou un dossier (ou plusieurs)
     à tester **Attention les sourcefiles ou folders doivent être donnés en chemin absolue**
* **options** : permet de lancer les scripts de test pour la validation des options du compilateur Deca (il peut être suivit de un ou plusieurs des arguments optionnels 
suivant)
    * **-n** : cette argument est optionnel, il permet de lancé uniquement le script de test de l'option -n du compilateur Deca
    * **-r** : cette argument est optionnel, il permet de lancé uniquement le script de test de l'option -r du compilateur Deca
    * **-p** : cette argument est optionnel, il permet de lancé uniquement le script de test de l'option -p du compilateur Deca
    * **-P** : cette argument est optionnel, il permet de lancé uniquement le script de test de l'option -P du compilateur Deca
    * **-d** : cette argument est optionnel, il permet de lancé uniquement le script de test de l'option -d du compilateur Deca
    * **-b** : cette argument est optionnel, il permet de lancé uniquement le script de test de l'option -b du compilateur Deca
    * **-v** : cette argument est optionnel, il permet de lancé uniquement le script de test de l'option -v du compilateur Deca
* **--all** : permet de lancer tous les tests des répertoires syntax, context et codegen. Lance également les tous les scripts de tests concernant les options du 
compilateur Deca. **Attention cette option est préemptive**
* **--mono** : permet de lancer Plant en mode mono-thread
* **--no-merge** : permet d'afficher des rapports non mergé (les statistiques gloables ne seront pas affichées)
* **--verobse** : permet d'activer le mode verbose (afficher imméditement les fichiers provoquant des erreurs). Les rapports en mode console ne seront pas affichés au 
fur et à mesure mais à la fin du traitement pour ne pas gêner le mode verbose.
* **--version** : permet d'afficher la version du compilateur. Plant se fermera ensuite. **Attention cette option est préemptive**

## Version

**3.0**

## Release note

**Branche de développement : plant**

Cette branche se base sur dev.

### Version 1.0
* Première version de Plant permettant uniquement de lancé l'outil avec les options de base (une seule à la fois).
    * syntax
    * context
    * codegen
* Cette version permet également de générer des rapports web.
* **Realease date** : 24/06/2015
* **Stable** : oui
* **Git**
    * Tag : plant-v1.0
    * Commit : efbf7be99839b978465e389f97cae35ad45630a5
        
### Version 1.1
* Version pemettant de fournir des fichiers spécifiques avec l'option -s.
* **Realease date** : 24/06/2015
* **Stable** : oui
* **Git**
    * **Tag** : plant-v1.1
    * **Commit** : 51ec8360f9700b1e1fca1d654677a938c8e88119
        
### Version 1.2
* Cette nouvelle version intègre le lancement des scripts permettant le test des options du compilateur Deca (lancement monolitique).
* **Realease date** : 24/06/2015
* **Stable** : non
* **Git**
    * **Tag** : plant-v1.2
    * **Commit** : 93c4ce7d6e23fa730d2d9c3b307057aaeba9a351
* **Known issues**
    * Impossible de lancer spécifiquement les tests
        
### Version 1.3
* Cette version intègre une complète refont de la gestion des options (pour satisfaire aux potentiels évolutions).
* Permet de fixer toutes les erreurs de la version précédente.
* Ajout des arguements pour lancer les options du compilateur Deca de manière unitaire.
    * **Realease date** : 24/06/2015
    * **Stable** : oui
    * **Git**
        * **Tag** : plant-v1.3
        * **Commit** : 6cbdb620dc9e7e08ab1aab28fdac700be9dff2f0
        
### Version 2.0
* Cette version apporte un grand nombre de nouveautés.
    * Multi-threading et argument.
    * Génération de rapport mergé et arguments.
    * Intégration de barres de progression.
    * Mode verbose (déconseillé en mode multi-threading).
    * Ordonnancement des fichiers testés dans les rapports.
* **Realease date** : 25/06/2015
* **Stable** : non
* **Git**
    * **Tag** : plant-v2.0
    * **Commit** : 8787f177b75118e7331a4a811ff4814ca8b32d93
* **Known issues**
    * Certains rapport ne sont pas générés
        
### Version 2.1
* Cette nouvelle version ne comprend aucune nouveauté concernant le lancements des tests.
* Ajout de l'argument --version
* **Realease date** : 25/06/2015
* **Stable** : non
* **Git**
    * **Tag** : plant-v2.1
    * **Commit** : 8f97b28839869341d4be9023bb4d9a65fe988e43
* **Known issues**
    * Certains rapport ne sont pas générés
        
### Version 2.2
* Cette nouvelle version intègre quelques nouveauté notables.
    * Réduction du nombre de décimales dans les pourcentages des statistiques.
    * Amélioration du mode verbose. Log beaucoup plus claires. Multi-threading supporté.
    * Modification du message de version et du comportement de cette argument.
* Cette version fix également toutes les issues des versions précédentes.
* **Realease date** : 25/06/2015
* **Stable** : oui
* **Git**
    * **Tag** : plant-v2.2
    * **Commit** : d58fdcbac02e84d948222aba1ef53c6f98d75fb4
    
### Version 3.0
* Cette nouvelle version améliore de manière significative la vitesse de traitement.
* Elle améiore également le mode verbose en ajoutant la gestion de plusieurs valeur de retour et l'affichage de message d'erreur.
* Les messages d'erreur sont également affichés dans les rapports lorsque le mode verbose est présent.
* Elle ajoute également un nouveau scope : decompile.
* Amélioration de l'instanciation des runners (modification de l'architecture).
* Cette nouvelle version est une rennaissance pour Plant dans le sens que désormais, il a vocation à instancier le plus de runner possible pour couvrir tous les 
besoins des développeurs en minimum de temps.
* **Realease date** : 26/06/2015
* **Stable** : oui
* **Git**
    * **Tag** : plant-v3.0
    * **Commit** : f52ea7f8f26df74d1d038610ad2e1f46ca2b5ba4
* **Known issues**
    * Lorsque le mode verbose n'est pas activé, les messages d'erreurs ne sont pas enregistré. Cela résulte en des messages 'null' dans la génération des rapports.
   
### Version 3.1
* Cette nouvelle version ajoute des informations, sur le retour de d'informations fournit par le script (message d'erreur et sortie standard et d'erreur), dans les 
rapports.
* Quelques améliorations graphique pour les barres de progression.
* Corrige le bug de la version précédente.
* **Realease date** : 26/06/2015
* **Stable** : oui
* **Git**
   * **Tag** : plant-v3.1
   * **Commit** : ca45c4363bb416354ef784f499f4b1675f7e5f8c

### Version 3.2
* Nouvelle gestion des scripts et des commandes shell.
* Ajout d'erreurs si un script ou un élément crucial (ima) n'est pas présent dans le classpath.
* **Realease date** : 28/06/2015
* **Stable** : oui
* **Git**
   * **Tag** : plant-v3.2
   * **Commit** : 78a5a13a8caa2b22cacd4bb7781c09faf20bb419
* **Known issues**
    * L'option -s ne fonctionne pas.
    * La génération des statistiques ne fonctionne pas sur certaines distributions linux (CentOS).
 
### Version 3.3 -> *version la plus stable*
* Version intégrant la refont du style des rapports web pour qu'ils soient plus sexy.
* Ajout de la vérification de la présence de la commandes bc.
* Correction des bugs connus de la version 3.2
* **Realease date** : 29/06/2015
* **Stable** : oui
* **Git**
   * **Tag** : plant-v3.3
   * **Commit** : 999a95361c2ed59c5ec105b3cbddfb5510487ca9
