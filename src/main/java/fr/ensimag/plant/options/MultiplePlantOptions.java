package fr.ensimag.plant.options;
import fr.ensimag.plant.data.definition.RunnerScope;

import java.util.ArrayList;
import java.util.List;
/**
 * Created by Tom on 24/6/15.
 */
public class MultiplePlantOptions<T> {
    private RunnerScope runnerScope;
    private List<T>     values;

    public MultiplePlantOptions(RunnerScope runnerScope) {
        this.runnerScope = runnerScope;
        this.values = new ArrayList<>();
    }

    public boolean add(T o) {
        return values.add(o);
    }

    /**
     * Gets runnerScope
     *
     * @return the runnerScope's value
     */
    public RunnerScope getRunnerScope() {
        return runnerScope;
    }

    public boolean isEmpty() {
        return values.isEmpty();
    }

    /**
     * Gets values
     *
     * @return the values's value
     */
    public List<T> getValues() {
        return values;
    }

    /**
     * Sets values
     *
     * @param values the value to set
     */
    public void setValues(List<T> values) {
        this.values = values;
    }
}
