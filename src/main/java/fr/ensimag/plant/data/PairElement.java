package fr.ensimag.plant.data;
import javax.swing.*;
/**
 * Created by Tom on 25/6/15.
 */
public class PairElement {
    JProgressBar progressBar;
    JLabel       label;

    public PairElement(String label, JProgressBar progressBar) {
        this.progressBar = progressBar;
        this.label = new JLabel(label);
    }

    /**
     * Gets progressBar
     *
     * @return the progressBar's value
     */
    public JProgressBar getProgressBar() {
        return progressBar;
    }

    /**
     * Gets label
     *
     * @return the label's value
     */
    public JLabel getLabel() {
        return label;
    }

    public int getProgress() {
        return progressBar.getValue();
    }

    public void setProgress(int n) {
        progressBar.setValue(n);
    }

    public int getMaximumProgress() {
        return progressBar.getMaximum();
    }

    public void setMaximumProgress(int n) {
        progressBar.setMaximum(n);
    }

    public boolean isFinished() {
        return getProgress() == getMaximumProgress();
    }

    public void setVisible(boolean visible) {
        label.setVisible(visible);
        progressBar.setVisible(visible);
    }
}
