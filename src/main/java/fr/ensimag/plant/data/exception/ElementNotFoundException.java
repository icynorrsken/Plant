package fr.ensimag.plant.data.exception;
/**
 * Created by Tom on 27/6/15.
 */
public class ElementNotFoundException extends RuntimeException {
    public ElementNotFoundException(String message) {
        super("Cannot find that script or command: '" + message + "'");
    }
}
