package fr.ensimag.plant.data;
import fr.ensimag.plant.data.definition.ScriptType;
import fr.ensimag.plant.data.definition.State;
/**
 * Created by Tom on 26/6/15.
 */
public class ScriptState {
    private int        exitStatus;
    private ScriptType scriptType;
    private State      state;
    private String     output;

    public ScriptState(int exitStatus, State state, String output) {
        this(exitStatus, state, ScriptType.GENERIC, output);
    }

    public ScriptState(int exitStatus, State state, ScriptType scriptType,
                       String output) {
        this.exitStatus = exitStatus;
        this.state = state;
        this.scriptType = scriptType;
        this.output = output;
    }

    /**
     * Gets output
     *
     * @return the output's value
     */
    public String getOutput() {
        return output;
    }

    /**
     * Gets state
     *
     * @return the state's value
     */
    public State getState() {
        return state;
    }

    /**
     * Gets exitStatus
     *
     * @return the exitStatus's value
     */
    public int getExitStatus() {
        return exitStatus;
    }

    public boolean isGeneric() {
        return scriptType.isGeneric();
    }

    public boolean isValid() {
        return scriptType.isValid();
    }

    public boolean isInvalid() {
        return scriptType.isInvalid();
    }
}
