package fr.ensimag.plant.data;
import fr.ensimag.plant.data.definition.RunnerScope;
import fr.ensimag.plant.data.exception.UnknownRunnerException;
import fr.ensimag.plant.report.Reportable;
import fr.ensimag.plant.report.SimpleReport;
import fr.ensimag.plant.report.WebReport;
import fr.ensimag.plant.report.mergeable.SimpleMergeableReport;
import fr.ensimag.plant.report.mergeable.WebMergeableReport;
import fr.ensimag.plant.runner.*;

import java.util.List;
/**
 * Created by Tom on 24/6/15.
 */
public class PlantFactory {
    public Reportable createReport(boolean web, boolean merge) {
        if (merge)
            return web ? new WebMergeableReport() : new SimpleMergeableReport();
        else
            return web ? new WebReport() : new SimpleReport();
    }

    public Runner createRunner(RunnerScope runnerScope, boolean web,
                               List sourceFiles, boolean merge,
                               boolean verbose) {
        if (sourceFiles.isEmpty())
            return createRunner(runnerScope, web, merge, verbose);
        else {
            AbstractRunner runner;
            switch (runnerScope) {
                case CONTEXT:
                    runner = new ContextRunner(createReport(web, merge),
                                               sourceFiles);
                    break;
                case SYNTAX:
                    runner = new SyntaxRunner(createReport(web, merge),
                                              sourceFiles);
                    break;
                case DECAC:
                    runner = new DecacRunner(createReport(web, merge),
                                             sourceFiles);
                    break;
                case DECOMPILE:
                    runner = new DecompileRunner(createReport(web, merge),
                                                 sourceFiles);
                    break;
                case OPTIONS:
                    runner = new OptionsRunner(createReport(web, merge),
                                               sourceFiles);
                    break;
                default:
                    throw new UnknownRunnerException(runnerScope);
            }
            runner.setVerbose(verbose);
            return runner;
        }
    }

    public Runner createRunner(RunnerScope runnerScope, boolean web,
                               boolean merge, boolean verbose) {
        AbstractRunner runner;
        switch (runnerScope) {
            case CONTEXT:
                runner = new ContextRunner(createReport(web, merge));
                break;
            case SYNTAX:
                runner = new SyntaxRunner(createReport(web, merge));
                break;
            case DECAC:
                runner = new DecacRunner(createReport(web, merge));
                break;
            case OPTIONS:
                runner = new OptionsRunner(createReport(web, merge));
                break;
            case DECOMPILE:
                runner = new DecompileRunner(createReport(web, merge));
                break;
            default:
                throw new UnknownRunnerException(runnerScope);
        }
        runner.setVerbose(verbose);
        return runner;
    }
}
