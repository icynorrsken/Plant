package fr.ensimag.plant.data.definition;
import fr.ensimag.plant.data.exception.UnknownRunnerException;
import fr.ensimag.plant.runner.*;
/**
 * Created by Tom on 26/6/15.
 */
public enum RunnerScope {
    DECAC("Codegen"),
    DECOMPILE("Decompile"),
    OPTIONS("Options"),
    SYNTAX("Syntax"),
    CONTEXT("Context");

    private String name;

    RunnerScope(String name) {
        this.name = name;
    }

    public static final RunnerScope getRunnerScope(Class clazz) {
        if (clazz == DecacRunner.class)
            return DECAC;
        else if (clazz == DecompileRunner.class)
            return DECOMPILE;
        else if (clazz == OptionsRunner.class)
            return OPTIONS;
        else if (clazz == SyntaxRunner.class)
            return SYNTAX;
        else if (clazz == ContextRunner.class)
            return CONTEXT;
        else
            throw new UnknownRunnerException(clazz.getClass());
    }

    /**
     * Gets name
     *
     * @return the name's value
     */
    public String getName() {
        return name;
    }
}
