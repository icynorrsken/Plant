package fr.ensimag.plant.data.definition;
/**
 * Created by Tom on 26/6/15.
 */
public enum ScriptType {
    VALID,
    INVALID,
    GENERIC;

    public boolean isGeneric() {
        return this == GENERIC;
    }

    public boolean isValid() {
        return this == VALID;
    }

    public boolean isInvalid() {
        return this == INVALID;
    }
}
