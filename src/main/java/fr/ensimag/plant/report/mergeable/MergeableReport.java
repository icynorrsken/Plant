package fr.ensimag.plant.report.mergeable;
import fr.ensimag.plant.statistics.Statistics;
/**
 * Created by Tom on 25/6/15.
 */
public interface MergeableReport {
    /**
     * Merge report together
     *
     * @param name
     * @param statistics
     */
    void merge(String name, Statistics statistics);

    /**
     * Special method for web report
     *
     * @param name
     */
    void webWrite(String name);
}
