package fr.ensimag.plant.report.mergeable;
import fr.ensimag.plant.statistics.Statistics;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
/**
 * Created by Tom on 25/6/15.
 */
public abstract class MergeableStatisticsReport {
    protected List<Statistics> statistics;
    protected StringBuilder    sb;
    private   NumberFormat     formatter;
    private   boolean          concurrent;

    public MergeableStatisticsReport(boolean concurrent) {
        statistics = new ArrayList<>();
        sb = new StringBuilder();
        formatter = new DecimalFormat("#0.00");
        this.concurrent = concurrent;
    }

    public boolean add(Statistics statistics) {
        return this.statistics.add(statistics);
    }

    protected String totalPercentOfPassedTest() {
        return formatter.format(((double) totalPassedTestNumber() /
                                 (double) totalTestRunNumber()) * 100);
    }

    protected int totalFailedTestNumber() {
        int total = 0;
        for (Statistics statistic : statistics) {
            total += statistic.getFailedTestNumber();
        }
        return total;
    }

    protected String totalPercentOfRunTest() {
        return formatter.format(((double) totalTestRunNumber() /
                                 (double) totalTestNumber()) * 100);
    }

    protected int totalTestNumber() {
        int total = 0;
        for (Statistics statistic : statistics) {
            total += statistic.getTestNumber();
        }
        return total;
    }

    protected int totalTestRunNumber() {
        int total = 0;
        for (Statistics statistic : statistics) {
            total += statistic.getTestRunNumber();
        }
        return total;
    }

    protected String totalDuration() {
        long millis = 0;
        if (concurrent) {
            for (Statistics statistic : statistics) {
                long durationTime = statistic.getDurationTime();
                if (durationTime > millis)
                    millis = durationTime;
            }
        } else {
            int start = 0, end = 0;
            for (Statistics statistic : statistics) {
                start += statistic.getStartTime();
                end += statistic.getEndTime();
            }
            millis = end - start;
        }
        return String.format("%d minutes %d seconds",
                             TimeUnit.MILLISECONDS.toMinutes(millis),
                             TimeUnit.MILLISECONDS.toSeconds(millis) -
                             TimeUnit.MINUTES.toSeconds(
                                     TimeUnit.MILLISECONDS.toMinutes(millis)));
    }

    protected int totalPassedTestNumber() {
        int total = 0;
        for (Statistics statistic : statistics) {
            total += statistic.getPassedTestNumber();
        }
        return total;
    }

    public abstract void createReport();

    protected abstract void write(String name);

    protected abstract void createTitle(String name);
}
