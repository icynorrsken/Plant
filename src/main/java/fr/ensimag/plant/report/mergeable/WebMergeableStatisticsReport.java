package fr.ensimag.plant.report.mergeable;
import fr.ensimag.plant.report.WebReport;

import java.io.*;
/**
 * Created by Tom on 25/6/15.
 */
public class WebMergeableStatisticsReport extends MergeableStatisticsReport {
    public WebMergeableStatisticsReport(boolean concurrent) {
        super(concurrent);
    }

    @Override
    public void createReport() {
        sb.append("<div id='statistics' width='100%' align='center'>");
        createTitle("Global Statistics");
        sb.append("<p>");
        sb.append("<b>Test passed:</b> ");
        sb.append(totalPassedTestNumber()).append("/")
                .append(totalTestRunNumber());
        sb.append(" (").append(totalPercentOfPassedTest()).append("%)");
        sb.append("</p>");
        sb.append("<p>");
        sb.append("<b>Test run:</b> ");
        sb.append(totalTestRunNumber()).append("/").append(totalTestNumber());
        sb.append(" (").append(totalPercentOfRunTest()).append("%)");
        sb.append("</p>");
        sb.append("<p>");
        sb.append("<b>Task executing in:</b> ");
        sb.append(totalDuration());
        sb.append("</p>");
        sb.append("<div class=\"alert alert-");
        if (totalFailedTestNumber() > 0)
            sb.append("danger\" role=\"alert\">")
                    .append("Oops, You need to fix some bugs !");
        else
            sb.append("success\" role=\"alert\">")
                    .append("Well done ! All tests have been passed!!");
        sb.append("</div>");
        sb.append("</div>");
        write(WebMergeableReport.NAME);
    }

    @Override
    protected void createTitle(String name) {
        sb.append("<div width='100%' align='center'><h1>").append(name)
                .append("</h1></div>");
    }

    @Override
    protected void write(String name) {
        File index = new File(
                System.getProperty("user.dir") + "/index-" + name + ".html");

        try (PrintWriter writer = new PrintWriter(
                new BufferedWriter(new FileWriter(index, true)))) {
            writer.println(sb.toString());
            writer.println(WebReport.htmlEnd());
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(
                "Site generated at: " + System.getProperty("user.dir") +
                "/index-" + name + ".html");
    }
}
