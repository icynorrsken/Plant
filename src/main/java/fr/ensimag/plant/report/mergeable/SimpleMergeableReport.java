package fr.ensimag.plant.report.mergeable;
import fr.ensimag.plant.data.FileWrapper;
import fr.ensimag.plant.report.SimpleReport;
import fr.ensimag.plant.statistics.Statistics;

import java.util.List;
/**
 * Created by Tom on 25/6/15.
 */
public class SimpleMergeableReport extends SimpleReport
        implements MergeableReport {
    @Override
    public synchronized void createReport(String name, Statistics statistics,
                                          List<FileWrapper> sourceFiles) {
        createTitle(name);
        createBody(initBody(sourceFiles));
        write(name);
    }

    @Override
    public void merge(String name, Statistics statistics) {
        sb.setLength(0);
        createStatistics(name, statistics);
        write(name);
    }

    @Override
    public void webWrite(String name) {
        throw new UnsupportedOperationException(
                "Simple report cannot createBody a web report");
    }
}
