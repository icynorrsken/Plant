package fr.ensimag.plant.report;
import fr.ensimag.plant.data.FileWrapper;
import fr.ensimag.plant.data.definition.ANSIColor;
import fr.ensimag.plant.data.definition.State;
import fr.ensimag.plant.statistics.Statistics;

import java.util.List;
import java.util.Map;
/**
 * Created by Tom on 22/6/15.
 */
public class SimpleReport extends AbstractReport {
    @Override
    protected void createTitle(String name) {
        sb.append("----------------------------------------\n");
        for (int i = 0; i < 20 - (name.length() / 2) - 1; i++) {
            sb.append(" ");
        }
        sb.append(name);
        for (int i = 0; i < 20 - (name.length() / 2); i++) {
            sb.append(" ");
        }
        sb.append("\n----------------------------------------\n\n");
    }

    @Override
    protected void write(String name) {
        System.out.println(sb.toString());
    }

    @Override
    protected void createBody(State sate, List<FileWrapper> files) {
        if (!files.isEmpty()) {
            String color = sate == State.PASSED ? ANSIColor.ANSI_GREEN.getCode()
                                                : sate == State.FAILED
                                                  ? ANSIColor.ANSI_RED.getCode()
                                                  : ANSIColor.ANSI_BLUE
                                                          .getCode();
            sb.append(color);
            createTitle(sate.toString());
            for (FileWrapper file : files) {
                sb.append(" ");
                sb.append(file.getFile().getAbsolutePath());
                if (file.getState() == State.FAILED) {
                    sb.append("\n\t");
                    sb.append(ANSIColor.ANSI_YELLOW.getCode());
                    sb.append(" ");
                    sb.append("Script Error Message: ");
                    sb.append(ANSIColor.ANSI_RESET.getCode());
                    sb.append(" ");
                    sb.append(file.getErrorMessage());
                    sb.append(" ");

                    sb.append("\n\t");
                    sb.append(ANSIColor.ANSI_YELLOW.getCode());
                    sb.append(" ");
                    sb.append("Standard and error output stream:\n\t");
                    sb.append(ANSIColor.ANSI_RESET.getCode());
                    sb.append(" ");
                    sb.append(file.getOutput().replace("\n", "\n\t "));
                    sb.append(" ");
                    sb.append(color);
                }
                sb.append("\n\n");
            }
        }
    }

    @Override
    protected void createBody(Map<State, List<FileWrapper>> map) {
        createBody(State.PASSED, map.get(State.PASSED));
        createBody(State.NOT_RUN, map.get(State.NOT_RUN));
        createBody(State.FAILED, map.get(State.FAILED));

        sb.append(ANSIColor.ANSI_RESET.getCode());
    }

    @Override
    protected void createStatistics(String name, Statistics statistics) {
        createTitle(name + " Statistics");
        sb.append("Test passed: ");
        sb.append(statistics.getPassedTestNumber()).append("/")
                .append(statistics.getTestRunNumber());
        sb.append(" (").append(statistics.percentOfPassedTest()).append("%)\n");
        sb.append("Test run: ");
        sb.append(statistics.getTestRunNumber()).append("/")
                .append(statistics.getTestNumber());
        sb.append(" (").append(statistics.percentOfRunTest()).append("%)\n");
        sb.append("Task executing in: ");
        sb.append(statistics.getDuration()).append("\n");
        if (statistics.getFailedTestNumber() > 0) {
            sb.append(ANSIColor.ANSI_RED.getCode());
            sb.append(failedScriptMessage());
            sb.append(ANSIColor.ANSI_RESET.getCode());
        }
    }
}
