package fr.ensimag.plant.report;
import fr.ensimag.plant.data.FileWrapper;
import fr.ensimag.plant.data.definition.State;
import fr.ensimag.plant.statistics.Statistics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * Created by Tom on 23/6/15.
 */
public abstract class AbstractReport implements Reportable {
    protected StringBuilder sb;

    public AbstractReport() {
        this.sb = new StringBuilder();
    }

    public static final String failedScriptMessage() {
        return "Be careful, you need to work again to fix all !";
    }

    public static final String passedScriptMessage() {
        return "Well done, all the tests passed !!";
    }

    @Override
    public synchronized void createReport(String name, Statistics statistics,
                                          List<FileWrapper> sourceFiles) {
        createTitle(name);
        createBody(initBody(sourceFiles));
        createStatistics(name, statistics);
        write(name);
    }

    protected final Map<State, List<FileWrapper>> initBody(
            List<FileWrapper> sourceFiles) {
        List<FileWrapper> passed = new ArrayList<>();
        List<FileWrapper> failed = new ArrayList<>();
        List<FileWrapper> notTested = new ArrayList<>();
        for (FileWrapper sourceFile : sourceFiles) {
            switch (sourceFile.getState()) {
                case WAIT:
                    break;
                case PASSED:
                    passed.add(sourceFile);
                    break;
                case FAILED:
                    failed.add(sourceFile);
                    break;
                case NOT_RUN:
                    notTested.add(sourceFile);
                    break;
            }
        }
        Map<State, List<FileWrapper>> map = new HashMap<>();
        map.put(State.PASSED, passed);
        map.put(State.FAILED, failed);
        map.put(State.NOT_RUN, notTested);
        return map;
    }

    protected abstract void createTitle(String name);

    protected abstract void write(String name);

    protected abstract void createBody(State sate, List<FileWrapper> files);

    protected abstract void createBody(Map<State, List<FileWrapper>> map);

    protected abstract void createStatistics(String name,
                                             Statistics statistics);
}
