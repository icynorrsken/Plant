package fr.ensimag.plant;

import fr.ensimag.plant.data.PlantFactory;
import fr.ensimag.plant.data.ProgressTasks;
import fr.ensimag.plant.data.definition.RunnerScope;
import fr.ensimag.plant.data.exception.ElementNotFoundException;
import fr.ensimag.plant.executor.CommandExecutor;
import fr.ensimag.plant.executor.Executor;
import fr.ensimag.plant.executor.ExecutorResult;
import fr.ensimag.plant.options.MainPlantOptions;
import fr.ensimag.plant.options.MultiplePlantOptions;
import fr.ensimag.plant.report.mergeable.MergeableStatisticsReport;
import fr.ensimag.plant.report.mergeable.SimpleMergeableStatisticsReport;
import fr.ensimag.plant.report.mergeable.WebMergeableStatisticsReport;
import fr.ensimag.plant.runner.Runner;

import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;
/**
 * Main class for the testing app
 */
public class Plant {
    public static final String VERSION = "3.3";
    private List<Runner>     runners;
    private MainPlantOptions plantOptions;
    private PlantFactory     plantFactory;

    public Plant() {
        plantFactory = new PlantFactory();
        runners = new ArrayList<>();
    }

    public static void main(String[] args) {

        Plant instance = new Plant();
        instance.start(args);
    }

    public void start(String[] args) {
        System.out.println("Launch with: " + Arrays.toString(args));

        // check if ima exist
        verifyRessources();

        MainPlantOptions plantOptions = new MainPlantOptions();

        try {
            plantOptions.computeOptions(args);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            System.exit(1);
        }

        setPlantOptions(plantOptions);

        init();
        run();
    }

    private void verifyRessources() {
        Map<String, Executor> executors = new HashMap<>();
        executors.put("ima", new CommandExecutor("which ima"));
        executors.put("bc", new CommandExecutor("which bc"));
        try {
            for (Entry<String, Executor> entry : executors.entrySet()) {
                ExecutorResult executorResult = entry.getValue().execute();
                if (!executorResult.getOutputStream().contains(entry.getKey()))
                    throw new ElementNotFoundException(entry.getKey());
            }
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    private void init() {
        for (Entry<RunnerScope, MultiplePlantOptions> entry : plantOptions
                .entrySet()) {
            MultiplePlantOptions r = entry.getValue();
            runners.add(plantFactory.createRunner(r.getRunnerScope(),
                                                  plantOptions.isWeb(),
                                                  r.getValues(),
                                                  plantOptions.isMergeable(),
                                                  plantOptions.isVerbose()));
        }
    }

    private void run() {
        plantOptions.printVersion();

        List<Thread> threads = new ArrayList<>();
        for (Runner runner : runners) {
            if (plantOptions.isMono()) {
                runner.run();
            } else {
                Thread t =
                        new Thread(runner, runner.getRunnerScope().getName());
                threads.add(t);
                t.start();
            }
        }
        if (!plantOptions.isMono()) {
            System.out.println(
                    "Number of active thread: " + Thread.activeCount());
            for (Thread thread : threads) {
                try {
                    thread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        if (plantOptions.isVerbose()) {
            for (Runner runner : runners) {
                runner.createReport(runner.getRunnerScope().getName());
            }
        }

        if (plantOptions.isMergeable()) {
            MergeableStatisticsReport statisticsReport =
                    plantOptions.isWeb() ? new WebMergeableStatisticsReport(
                            !plantOptions.isMono())
                                         : new SimpleMergeableStatisticsReport(
                                                 !plantOptions.isMono());
            for (Runner runner : runners) {
                runner.mergeReport(runner.getRunnerScope().getName());
                statisticsReport.add(runner.getStatistics());
            }
            System.out.println("Start generating mergeable statistics report");
            statisticsReport.createReport();
        }

        ProgressTasks.getInstance().closeWindow();
    }

    /**
     * Sets plantOptions
     *
     * @param plantOptions the value to set
     */
    private void setPlantOptions(MainPlantOptions plantOptions) {
        this.plantOptions = plantOptions;
    }
}
