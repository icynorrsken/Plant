package fr.ensimag.plant.runner;
import fr.ensimag.plant.data.FileWrapper;
import fr.ensimag.plant.data.ProgressTasks;
import fr.ensimag.plant.data.ScriptState;
import fr.ensimag.plant.data.definition.State;
import fr.ensimag.plant.data.exception.ElementNotFoundException;
import fr.ensimag.plant.executor.ExecutorResult;
import fr.ensimag.plant.executor.ScriptExecutor;
import fr.ensimag.plant.report.Reportable;
import fr.ensimag.plant.statistics.StatisticImpl;

import java.io.File;
import java.io.IOException;
import java.util.List;
/**
 * Created by Tom on 24/6/15.
 */
public class OptionsRunner extends AbstractRunner {
    private static final String   ARG_N        =
            PREFIX_SCRIPT_PATH + "options/arg-n.sh";
    private static final String   ARG_P        =
            PREFIX_SCRIPT_PATH + "options/arg-p.sh";
    private static final String   ARG_PARALLEL =
            PREFIX_SCRIPT_PATH + "options/arg-parallel.sh";
    private static final String   ARG_B        =
            PREFIX_SCRIPT_PATH + "options/arg-b.sh";
    private static final String   ARG_D        =
            PREFIX_SCRIPT_PATH + "options/arg-d.sh";
    private static final String   ARG_R        =
            PREFIX_SCRIPT_PATH + "options/arg-r.sh";
    private static final String   ARG_V        =
            PREFIX_SCRIPT_PATH + "options/arg-v.sh";
    private static final String[] ALL          =
            new String[]{ARG_N, ARG_P, ARG_PARALLEL, ARG_B, ARG_D, ARG_R,
                         ARG_V};

    public OptionsRunner(Reportable report) {
        super();
        this.report = report;
        statistics = new StatisticImpl(ALL.length);
        for (String script : ALL) {
            sourceFiles.add(new FileWrapper(new File(script)));
        }
        initProgressBar(ALL.length);
    }

    public OptionsRunner(Reportable report, List<String> decacOptions) {
        this.report = report;
        statistics = new StatisticImpl(decacOptions.size());
        for (String decacOption : decacOptions) {
            switch (decacOption) {
                case "-n":
                    sourceFiles.add(new FileWrapper(new File(ARG_N)));
                    break;
                case "-p":
                    sourceFiles.add(new FileWrapper(new File(ARG_P)));
                    break;
                case "-P":
                    sourceFiles.add(new FileWrapper(new File(ARG_PARALLEL)));
                    break;
                case "-v":
                    sourceFiles.add(new FileWrapper(new File(ARG_V)));
                    break;
                case "-b":
                    sourceFiles.add(new FileWrapper(new File(ARG_B)));
                    break;
                case "-d":
                    sourceFiles.add(new FileWrapper(new File(ARG_D)));
                    break;
                case "-r":
                    sourceFiles.add(new FileWrapper(new File(ARG_R)));
                    break;
            }
        }
        initProgressBar(decacOptions.size());
    }

    @Override
    protected ScriptState run(FileWrapper sourceFile,
                              ScriptExecutor scriptExecutor) {
        State returnState = State.WAIT;
        ExecutorResult executorResult = new ExecutorResult();
        try {
            String path = sourceFile.getFile().getAbsolutePath();
            scriptExecutor.setScript(path);
            executorResult = scriptExecutor.execute();
            returnState = executorResult.getExitStatus() == 0 ? State.PASSED
                                                              : State.FAILED;
        } catch (IOException | InterruptedException |
                ElementNotFoundException e) {
            e.printStackTrace();
            returnState = State.FAILED;
        } finally {
            if (returnState == State.WAIT)
                returnState = State.FAILED;
            return new ScriptState(executorResult.getExitStatus(), returnState,
                                   executorResult.getOutputStream());
        }
    }

    private void initProgressBar(int taskNumber) {
        ProgressTasks progressTasks = ProgressTasks.getInstance();
        String name = getRunnerScope().getName();
        progressTasks.put(name);
        progressTasks.init(name, taskNumber);
    }

    @Override
    protected String getSuffix() {
        return "options/";
    }

    @Override
    protected String getValidScriptPath() {
        return null;
    }

    @Override
    protected String getInvalidScriptPath() {
        return null;
    }
}
