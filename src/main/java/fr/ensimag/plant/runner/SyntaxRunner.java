package fr.ensimag.plant.runner;
import fr.ensimag.plant.data.FileWrapper;
import fr.ensimag.plant.data.ScriptState;
import fr.ensimag.plant.data.definition.ScriptType;
import fr.ensimag.plant.data.definition.State;
import fr.ensimag.plant.data.exception.ElementNotFoundException;
import fr.ensimag.plant.executor.ExecutorResult;
import fr.ensimag.plant.executor.ScriptExecutor;
import fr.ensimag.plant.report.Reportable;

import java.io.File;
import java.io.IOException;
import java.util.List;
/**
 * Created by Tom on 23/6/15.
 */
public class SyntaxRunner extends AbstractRunner {
    public SyntaxRunner(Reportable report) {
        super(report);
    }

    public SyntaxRunner(Reportable report, List<File> sourcesFiles) {
        super(report, sourcesFiles);
    }

    @Override
    public ScriptState run(FileWrapper sourceFile,
                           ScriptExecutor scriptExecutor) {
        State returnState = State.WAIT;
        ScriptType scriptType = ScriptType.GENERIC;
        ExecutorResult executorResult = new ExecutorResult();
        try {
            String path = sourceFile.getFile().getAbsolutePath();
            if (!path.contains("invalid")) {
                scriptExecutor.setScript(getValidScriptPath());
                scriptExecutor.setArguments(path);
                scriptType = ScriptType.VALID;
            } else {
                String expected = path.replace(".deca", ".expected");
                File file = new File(expected);
                if (file.exists()) {
                    scriptExecutor.setScript(getInvalidScriptPath());
                    scriptExecutor.setArguments(path);
                } else
                    returnState = State.NOT_RUN;
                scriptType = ScriptType.INVALID;
            }
            if (returnState != State.NOT_RUN) {
                executorResult = scriptExecutor.execute();
                returnState = executorResult.getExitStatus() == 0 ? State.PASSED
                                                                  : State
                                      .FAILED;
            }
        } catch (IOException | InterruptedException |
                ElementNotFoundException e) {
            e.printStackTrace();
            returnState = State.FAILED;
        } finally {
            if (returnState == State.WAIT)
                returnState = State.FAILED;
            return new ScriptState(executorResult.getExitStatus(), returnState,
                                   scriptType,
                                   executorResult.getOutputStream());
        }
    }

    @Override
    protected String getSuffix() {
        return "syntax/";
    }

    @Override
    protected String getValidScriptPath() {
        return PREFIX_SCRIPT_PATH + "synth_valid.sh";
    }

    @Override
    protected String getInvalidScriptPath() {
        return PREFIX_SCRIPT_PATH + "synth_invalid.sh";
    }
}
