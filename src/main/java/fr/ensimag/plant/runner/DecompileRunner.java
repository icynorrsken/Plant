package fr.ensimag.plant.runner;
import fr.ensimag.plant.data.FileWrapper;
import fr.ensimag.plant.data.ScriptState;
import fr.ensimag.plant.data.definition.State;
import fr.ensimag.plant.data.exception.ElementNotFoundException;
import fr.ensimag.plant.executor.ExecutorResult;
import fr.ensimag.plant.executor.ScriptExecutor;
import fr.ensimag.plant.report.Reportable;

import java.io.File;
import java.io.IOException;
import java.util.List;
/**
 * Created by Tom on 26/6/15.
 */
public class DecompileRunner extends AbstractRunner {
    public DecompileRunner(Reportable report) {
        super(report);
    }

    public DecompileRunner(Reportable report, List<File> sourcesFiles) {
        super(report, sourcesFiles);
    }

    @Override
    public ScriptState run(FileWrapper sourceFile,
                           ScriptExecutor scriptExecutor) {
        State returnState = State.WAIT;
        ExecutorResult executorResult = null;
        try {
            String path = sourceFile.getFile().getAbsolutePath();
            scriptExecutor.setScript(getValidScriptPath());
            scriptExecutor.setArguments(path);
            executorResult = scriptExecutor.execute();
            returnState = executorResult.getExitStatus() == 0 ? State.PASSED
                                                              : State.FAILED;
        } catch (IOException | InterruptedException |
                ElementNotFoundException e) {
            e.printStackTrace();
            returnState = State.FAILED;
        } finally {
            if (returnState == State.WAIT)
                returnState = State.FAILED;
            return new ScriptState(executorResult.getExitStatus(), returnState,
                                   executorResult.getOutputStream());
        }
    }

    @Override
    protected String getSuffix() {
        return "codegen/";
    }

    @Override
    protected String getValidScriptPath() {
        return PREFIX_SCRIPT_PATH + "decompile.sh";
    }

    @Override
    protected String getInvalidScriptPath() {
        return PREFIX_SCRIPT_PATH + "decompile.sh";
    }
}
