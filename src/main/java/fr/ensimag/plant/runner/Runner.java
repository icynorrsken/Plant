package fr.ensimag.plant.runner;
import fr.ensimag.plant.data.definition.RunnerScope;
import fr.ensimag.plant.statistics.Statistics;
/**
 * Created by Tom on 22/6/15.
 */
public interface Runner extends Runnable {
    /**
     * Gets statistics
     *
     * @return the statistics's value
     */
    Statistics getStatistics();

    /**
     * Create report
     *
     * @param name
     */
    void createReport(String name);

    /**
     * Merge report in file Method can only be called by a mergeable report
     *
     * @param name
     */
    void mergeReport(String name);

    /**
     * Return the scope of the runner It's the runner name without the extension
     * Runner
     *
     * @return
     */
    RunnerScope getRunnerScope();
}
