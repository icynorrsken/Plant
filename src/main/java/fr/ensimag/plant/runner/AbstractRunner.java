package fr.ensimag.plant.runner;
import fr.ensimag.plant.data.FileWrapper;
import fr.ensimag.plant.data.ProgressTasks;
import fr.ensimag.plant.data.ScriptState;
import fr.ensimag.plant.data.definition.ANSIColor;
import fr.ensimag.plant.data.definition.RunnerScope;
import fr.ensimag.plant.data.definition.ScriptErrorMessage;
import fr.ensimag.plant.data.definition.State;
import fr.ensimag.plant.executor.ScriptExecutor;
import fr.ensimag.plant.report.Reportable;
import fr.ensimag.plant.report.mergeable.MergeableReport;
import fr.ensimag.plant.statistics.StatisticImpl;
import fr.ensimag.plant.statistics.Statistics;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
/**
 * Created by Tom on 22/6/15.
 */
public abstract class AbstractRunner implements Runner {
    protected static final String PREFIX_SCRIPT_PATH =
            System.getProperty("user.dir") + "/src/test/script/";
    private static final   String PREFIX_PATH        =
            System.getProperty("user.dir") + "/src/test/deca/";
    protected Reportable        report;
    protected Statistics        statistics;
    protected List<FileWrapper> sourceFiles;
    protected boolean           verbose;

    protected AbstractRunner() {
        super();
        verbose = false;
        sourceFiles = new ArrayList<>();
    }

    public AbstractRunner(Reportable report) {
        this();
        this.report = report;
        statistics = new StatisticImpl();
        initSourceFiles(getPath());
    }

    public AbstractRunner(Reportable report, List<File> sourcesFiles) {
        this();
        this.report = report;
        statistics = new StatisticImpl();
        for (File sourcesFile : sourcesFiles) {
            initSourceFiles(sourcesFile.getPath());
        }
    }

    @Override
    public void run() {
        System.out
                .println("Start executing task: " + getRunnerScope().getName());
        statistics.start();
        Iterator<FileWrapper> it = sourceFiles.iterator();
        int progress = 0;
        while (it.hasNext()) {
            FileWrapper file = it.next();
            ScriptState result = run(file, new ScriptExecutor());

            if (result.getState() != State.WAIT)
                file.setState(result.getState());

            switch (result.getState()) {
                case WAIT:
                case NOT_RUN:
                    break;
                case PASSED:
                    incrementPassedTestNumber();
                    incrementTestRunNumber();
                    break;
                case FAILED:
                    incrementFailedTestNumber();
                    incrementTestRunNumber();
                    String errorMessage;
                    if (result.isGeneric())
                        errorMessage = ScriptErrorMessage
                                .getErrorMessage(getRunnerScope(),
                                                 result.getExitStatus());
                    else
                        errorMessage = ScriptErrorMessage
                                .getErrorMessage(getRunnerScope(),
                                                 result.getExitStatus(),
                                                 result.isValid());
                    file.setErrorMessage(errorMessage);
                    file.setOutput(result.getOutput());
                    if (verbose)
                        printScriptErrorMessage(getRunnerScope(), file);
                    break;
            }

            ProgressTasks.getInstance()
                    .notifyUpdate(getRunnerScope().getName(), ++progress);
        }
        statistics.end();
        if (!verbose)
            createReport(getRunnerScope().getName());
    }

    @Override
    public void mergeReport(String name) {
        // Merge is possible cause of the pre condition
        ((MergeableReport) report)
                .merge(getRunnerScope().getName(), statistics);
    }

    @Override
    public void createReport(String name) {
        System.out.println("Start generating report: " + name);
        report.createReport(name, statistics, sourceFiles);
    }

    public int incrementTestNumber() {
        return statistics.incrementTestNumber();
    }

    public int incrementTestRunNumber() {
        return statistics.incrementTestRunNumber();
    }

    public int incrementPassedTestNumber() {
        return statistics.incrementPassedTestNumber();
    }

    public int incrementFailedTestNumber() {
        return statistics.incrementFailedTestNumber();
    }

    /**
     * Print the error message and return that message to print it in report
     *
     * @param runner
     * @param file
     *
     * @return
     */
    private final void printScriptErrorMessage(RunnerScope runner,
                                               FileWrapper file) {
        StringBuilder sb = new StringBuilder();
        sb.append("Call with scope: ");
        sb.append(ANSIColor.ANSI_BLUE.getCode());
        sb.append(" ");
        sb.append(runner);
        sb.append(" ");
        sb.append(ANSIColor.ANSI_RESET.getCode());
        sb.append(" -> ");
        sb.append(ANSIColor.ANSI_RED.getCode());
        sb.append(" ");
        sb.append(file.getFile().getAbsolutePath());
        sb.append("\n\t");
        sb.append(ANSIColor.ANSI_YELLOW.getCode());
        sb.append(" ");
        sb.append("Error Message: ");
        sb.append(ANSIColor.ANSI_RESET.getCode());
        sb.append(" ");
        sb.append(file.getErrorMessage());
        sb.append("\n");
        System.out.println(sb.toString());
    }

    private void initSourceFiles(String path) {
        File dir = new File(path);
        List<File> files = initSourceFiles(dir);
        for (File file : files) {
            sourceFiles.add(new FileWrapper(file));
        }
        ProgressTasks progressTasks = ProgressTasks.getInstance();
        String name = getRunnerScope().getName();
        progressTasks.put(name);
        progressTasks.init(name, sourceFiles.size());
    }

    private List<File> initSourceFiles(File dir) {
        List<File> files = new ArrayList<>();

        if (dir.isDirectory()) {
            for (File file : dir.listFiles()) {
                files.addAll(initSourceFiles(file));
            }
        } else {
            if (dir.getPath().endsWith(".deca")) {
                files.add(dir);
                incrementTestNumber();
            }
        }

        return files;
    }

    protected abstract String getSuffix();

    protected abstract String getValidScriptPath();

    protected abstract String getInvalidScriptPath();

    /**
     * Run the given test
     */
    protected abstract ScriptState run(FileWrapper sourceFile,
                                       ScriptExecutor scriptExecutor);

    /**
     * Gets statistics
     *
     * @return the statistics's value
     */
    @Override
    public Statistics getStatistics() {
        return statistics;
    }

    private String getPath() {
        return PREFIX_PATH + getSuffix();
    }

    @Override
    public RunnerScope getRunnerScope() {
        return RunnerScope.getRunnerScope(getClass());
    }

    /**
     * Sets verbose
     *
     * @param verbose the value to set
     */
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }
}
