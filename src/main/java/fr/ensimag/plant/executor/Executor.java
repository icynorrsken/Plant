package fr.ensimag.plant.executor;
import java.io.IOException;
/**
 * Created by Tom on 27/6/15.
 */
public interface Executor {
    ExecutorResult execute() throws InterruptedException, IOException;
}
