package fr.ensimag.plant.executor;
import java.io.IOException;
/**
 * Created by Tom on 27/6/15.
 */
public class CommandExecutor extends AbstractExecutor {
    private String command;

    public CommandExecutor(String command) {
        this.command = command;
    }

    @Override
    protected Process exec() throws IOException {
        return Runtime.getRuntime().exec(command);
    }
}
