package fr.ensimag.plant.executor;
/**
 * Created by Tom on 27/6/15.
 */
public class ExecutorResult {
    private int    exitStatus;
    private String outputStream;

    public ExecutorResult(int exitStatus, String outputStream) {
        this.exitStatus = exitStatus;
        this.outputStream = outputStream;
    }

    public ExecutorResult() {
        this(1, "-- Nothing --");
    }

    /**
     * Gets exitStatus
     *
     * @return the exitStatus's value
     */
    public int getExitStatus() {
        return exitStatus;
    }

    /**
     * Gets outputStream
     *
     * @return the outputStream's value
     */
    public String getOutputStream() {
        return outputStream;
    }
}
