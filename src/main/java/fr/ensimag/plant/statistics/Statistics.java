package fr.ensimag.plant.statistics;
/**
 * Created by Tom on 22/6/15.
 */
public interface Statistics {
    /**
     * Increment the number of existing test
     */
    int incrementTestNumber();

    /**
     * Increment the number of test run
     */
    int incrementTestRunNumber();

    /**
     * Increment the number of test successfully run
     */
    int incrementPassedTestNumber();

    /**
     * Increment the number of test unsuccessfully run
     */
    int incrementFailedTestNumber();

    /**
     * Compute the percentage of passed test
     *
     * @return
     */
    String percentOfPassedTest();

    /**
     * Compute the percentage of done test
     *
     * @return
     */
    String percentOfRunTest();

    /**
     * Gets testNumber
     *
     * @return the testNumber's value
     */
    int getTestNumber();

    /**
     * Gets testRunNumber
     *
     * @return the testRunNumber's value
     */
    int getTestRunNumber();

    /**
     * Gets passedTestNumber
     *
     * @return the passedTestNumber's value
     */
    int getPassedTestNumber();

    /**
     * Gets failedTestNumber
     *
     * @return the failedTestNumber's value
     */
    int getFailedTestNumber();

    /**
     * Start time counter
     */
    void start();

    /**
     * End time counter
     */
    void end();

    /**
     * Get the duration Method start and end have to be called before
     *
     * @return the duration
     */
    String getDuration();

    /**
     * Get the start time
     */
    long getStartTime();

    /**
     * Get the end time
     */
    long getEndTime();

    /**
     * Get duration in milli seconds
     *
     * @return
     */
    long getDurationTime();
}
