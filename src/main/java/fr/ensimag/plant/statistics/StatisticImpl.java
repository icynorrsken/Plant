package fr.ensimag.plant.statistics;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
/**
 * Created by Tom on 22/6/15.
 */
public class StatisticImpl implements Statistics {
    private AtomicInteger testNumber;
    private AtomicInteger testRunNumber;
    private AtomicInteger passedTestNumber;
    private AtomicInteger failedTestNumber;
    private long          start;
    private long          end;
    private NumberFormat  formatter;

    /**
     * Constuctor
     */
    public StatisticImpl() {
        this(0);
    }

    public StatisticImpl(int testNumber) {
        super();
        this.testNumber = new AtomicInteger(testNumber);
        testRunNumber = new AtomicInteger(0);
        passedTestNumber = new AtomicInteger(0);
        failedTestNumber = new AtomicInteger(0);
        formatter = new DecimalFormat("#0.00");
    }

    @Override
    public void start() {
        start = System.currentTimeMillis();
    }

    @Override
    public void end() {
        end = System.currentTimeMillis();
    }

    @Override
    public String percentOfPassedTest() {
        return formatter.format(((double) passedTestNumber.get() /
                                 (double) testRunNumber.get()) * 100);
    }

    @Override
    public String percentOfRunTest() {
        return formatter.format(((double) testRunNumber.get() /
                                 (double) testNumber.get()) * 100);
    }

    @Override
    public int incrementTestNumber() {
        return testNumber.incrementAndGet();
    }

    @Override
    public int incrementTestRunNumber() {
        return testRunNumber.incrementAndGet();
    }

    @Override
    public int incrementPassedTestNumber() {
        return passedTestNumber.incrementAndGet();
    }

    @Override
    public int incrementFailedTestNumber() {
        return failedTestNumber.incrementAndGet();
    }

    @Override
    public String getDuration() {
        long millis = end - start;
        return String.format("%d minutes %d seconds",
                             TimeUnit.MILLISECONDS.toMinutes(millis),
                             TimeUnit.MILLISECONDS.toSeconds(millis) -
                             TimeUnit.MINUTES.toSeconds(
                                     TimeUnit.MILLISECONDS.toMinutes(millis)));
    }

    @Override
    public long getDurationTime() {
        return end - start;
    }

    @Override
    public long getStartTime() {
        return start;
    }

    @Override
    public long getEndTime() {
        return end;
    }

    /**
     * Gets testNumber
     *
     * @return the testNumber's value
     */
    @Override
    public int getTestNumber() {
        return testNumber.get();
    }

    /**
     * Gets testRunNumber
     *
     * @return the testRunNumber's value
     */
    @Override
    public int getTestRunNumber() {
        return testRunNumber.get();
    }

    /**
     * Gets passedTestNumber
     *
     * @return the passedTestNumber's value
     */
    @Override
    public int getPassedTestNumber() {
        return passedTestNumber.get();
    }

    /**
     * Gets failedTestNumber
     *
     * @return the failedTestNumber's value
     */
    @Override
    public int getFailedTestNumber() {
        return failedTestNumber.get();
    }
}
